NOTE: Hamster is being maintained at the moment. 
Details here: http://projecthamster.wordpress.com/2013/05/13/hamster-is-looking-for-a-new-maintainer/



Hamster can be found packaged in most distributions. The instructions below
are for installing from sources for testing and development purposes.

h1. GNOME 2.x support

GNOME 3 has now been around for almost 3 years now and in order to be able
to move forward, I am getting rid of all the cruft. That includes dropping the
applet (sorely missed, there are no applets in gnome 3) as i can't test anymore
and preferring the
"shell-extension":https://github.com/projecthamster/shell-extension instead.

If you are running GNOME 2.x, this branch is the last good state before
the start of cleanup: https://github.com/projecthamster/hamster/tree/gnome_2x
Switch to it in your github client and proceed with the instructions.
I have no plans to maintain the branch just now and am suggesting that everybody
bites the bullet and moves on :)
The good news is that there aren't that many new things in hamster as the most
energy has been spent trying to get something that works just like the applet
back to the desktop (~60% there with the extension).


h1. hamster-applet -> hamster-time-tracker clean up

Previously hamster was installed everywhere under @hamster-applet@. As
the applet is long gone, the paths and file names have changed to
@hamster-time-tracker@. To clean up previous install follow these steps:

pre. git checkout d140d45f105d4ca07d4e33bcec1fae30143959fe
./waf configure build --prefix=/usr
sudo ./waf uninstall

h1. Dependencies

Debian-based: @apt-get install git-core gettext intltool python-gconf@
RPM-based: @yum install git-core gettext intltool gnome-python2-gconf@

h1. Installing

pre. ./waf configure build --prefix=/usr
sudo ./waf install

After that make sure that the old daemons aren't roaming around:

pre. killall hamster-service && killall hamster-windows-service

Make sure gconf settings are owned by you and not root:

pre. cd ~/.gconf/apps/
ls -l | grep hamster-time-tracker
sudo chown -R <user>:<group> hamster-time-tracker

Now restart your panels/dockies and you should be able to add hamster to your panel!

h2. Required

pre. sudo apt-get install python-requests python-beaker

h1. Screenshots

!https://raw.github.com/Americas/hamster/master/entry.png!

!https://raw.github.com/Americas/hamster/master/settings.png!

h1. Additional settings

To enable/disable the icon glow: 

pre. $ gconftool-2 --set "/apps/hamster-time-tracker/icon_glow" --type bool "false"
$ gconftool-2 --set "/apps/hamster-time-tracker/icon_glow" --type bool "true"

To enable/disable the activity label:

pre. $ gconftool-2 --set "/apps/hamster-time-tracker/show_label" --type bool "true"
$ gconftool-2 --set "/apps/hamster-time-tracker/show_label" --type bool "false"

To set the maximum label length:

pre. $ gconftool-2 --set "/apps/hamster-time-tracker/label_length" --type int "20"
